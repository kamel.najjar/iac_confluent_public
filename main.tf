module "confluent_kafka_topics" {
  source = "./modules/ccloud_topics_module"

  kafka_id            = var.kafka_id
  kafka_rest_endpoint = var.kafka_rest_endpoint
  kafka_api_key       = var.kafka_api_key
  kafka_api_secret    = var.kafka_api_secret

  topics              = jsondecode(file("./editique-ccloud-config/${var.input_environnement}/${var.input_json_filename}"))
}